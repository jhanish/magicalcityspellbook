# Magical City - Python : Project Times Tables
# CODE CHALLENGE # 7


# CHALLENGE 3...
#
# Create a function called listReverse() that takes a list as an argument, and returns
# the same list, with the order reversed.  (and don't use the reverse() function!)
# Use the data and print statements below to test out your function.

# Clue 1: You can iterate through the passed in list with a for-in loop.
# Clue 2: You can use list.insert() to put items at the beginning of a list.
# Clue 3: You will need to start with a new empty list in your function.  new_list = []

# <----- PUT YOUR listReverse() FUNCTION HERE ------>

def listReverse(list_to_reverse):
    new_list = []

    for item in list_to_reverse:
        new_list.insert(0, item)

    return new_list


# <----- PUT YOUR listReverse() FUNCTION HERE ------>

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list2 = ['George', 'Paul', 'John', 'Ringo']

print(list1)
print(listReverse(list1))
print(list2)
print(listReverse(list2))




# CHALLENGE 2...
#
# Create a function called listRandomize() that takes 1 list as a parameter,  and returns a list, of list 1's items, in random order.
#
# Clue #1: random.choice() is your friend
# Clue #2: you can remove an item from a list with list.remove(item)
#
# Challenge 2 Bonus
# ------------------
# If you removed items from the passed in list, you will notice that in the last two print statements below, the list is missing it's
# members.  This happens even if you try copying it to a new variable like this: my_list = list_in_argument.
# What if we didn't want this behavior, what could we do?
#
# Clue #1:  Look into copy.deepcopy()
# 

import random
import copy

# <----- PUT YOUR listRandomize() FUNCTION HERE ------>

def listRandomize(list_to_randomize):
    randomized_list = []

    while len(list_to_randomize) > 0:
        item = random.choice(list_to_randomize)
        randomized_list.append(item)
        list_to_randomize.remove(item)
        
    return randomized_list




# <----- PUT YOUR listRandomize() FUNCTION HERE ------>

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list2 = ['George', 'Paul', 'John', 'Ringo']

print("<-=-=-=-=-=-=-=-=-=-=-=-=->")
print(list1)
print(listRandomize(list1))
print(list2)
print(listRandomize(list2))
print(list1)
print(list2)





# CHALLENGE 1...  

# Add code for a list of numbers, which contain ONLY the numbers of the times
# tables you currently need help with.  For example, if you really feel
# like you have the 0, 1, and 10 times tables, you're going to include
# a list like this: 
#
# times_tables_to_quiz = [2, 3, 4, 5, 6, 7, 8, 9, 11, 12]
#
# or if you really only needed help with the 6, 7, and 8 times tables...
#
# times_tables_to_quiz = [6, 7, 8]
#
# Put this list in your Quizzinator app, and use it to generate the 
# first factor of the quiz facts, so Quizzinator only quizzes you on
# the times tables you need.  
#
# Hints:
#   Hint #1: random.choice() is your friend again.

#   Hint #2: You need to replace the random.randint() function.

#   Hint #3: Remove the randint() call, and replace it with 
#            random.choice(times_tables_to_quiz) if you named the 
#            times tables list the way it appears above.  Remember,
#            the times_tables_to_quiz list must come BEFORE the 
#            call to random.choice(), so the list exists for you to
#            pass in! 




