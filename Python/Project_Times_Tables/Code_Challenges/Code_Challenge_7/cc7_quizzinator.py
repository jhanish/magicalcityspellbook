from colorama import Fore, Back, Style, init
import random
from magicsound import magicsound

times_tables_to_quiz = [6, 7, 8]

init()

def color_input(prompt):
    print(prompt, end='')
    return input('')


print(f"\n{Fore.MAGENTA}         Welcome to")
print(f"{Fore.CYAN} Your Secret Weapon for Times Table Mastery...{Fore.RESET}\n")

print(f"{Fore.MAGENTA}░██████╗░██╗░░░██╗██╗███████╗███████╗██╗███╗░░██╗░█████╗░████████╗░█████╗░██████╗░")
print(f"{Fore.CYAN}██╔═══██╗██║░░░██║██║╚════██║╚════██║██║████╗░██║██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗")
print(f"{Fore.YELLOW}██║██╗██║██║░░░██║██║░░███╔═╝░░███╔═╝██║██╔██╗██║███████║░░░██║░░░██║░░██║██████╔╝")
print(f"{Fore.GREEN}╚██████╔╝██║░░░██║██║██╔══╝░░██╔══╝░░██║██║╚████║██╔══██║░░░██║░░░██║░░██║██╔══██╗")
print(f"{Fore.RED}░╚═██╔═╝░╚██████╔╝██║███████╗███████╗██║██║░╚███║██║░░██║░░░██║░░░╚█████╔╝██║░░██║")
print(f"{Fore.MAGENTA}░░░╚═╝░░░░╚═════╝░╚═╝╚══════╝╚══════╝╚═╝╚═╝░░╚══╝╚═╝░░╚═╝░░░╚═╝░░░░╚════╝░╚═╝░░╚═╝")
print(f"{Fore.RESET}")

magicsound('qs - welcome to quizzinator 3.wav')

while True:
    try:
        total_question_count = int(color_input(f"{Fore.CYAN}Please enter the number of problems you'd like to do today: {Fore.YELLOW}"))
        break
    except ValueError:
        print(f"{Fore.RED}Sorry, I didn't understand that.  I need a number!{Fore.RESET}\n")
        magicsound('qs - attention 6.wav')

games_played = 0
grand_total_right = 0
grand_total_wrong = 0

while True: # The big game loop

    print(f"\n{Fore.GREEN}Ok, {total_question_count} questions it is!\n{Fore.YELLOW}Let's go!\n")

    questions_left = total_question_count
    correct_answers = 0
    incorrect_answers = 0

    while questions_left > 0:

        factor1 = random.choice(times_tables_to_quiz)
        factor2 = random.randint(0, 12)
        product = factor1 * factor2

        # get a random bit, and see if it's 1.  If not, ignore.
        if random.getrandbits(1) == 1:
            # swap the two factors 
            factor1, factor2 = factor2, factor1

        fact_display = f"{Fore.YELLOW}{factor1} {Fore.GREEN}x {Fore.YELLOW}{factor2} {Fore.GREEN}= {Fore.WHITE}"

        while True:
            try:
                guess = int(color_input(f"{Fore.CYAN}SOLVE: {fact_display}"))
                break
            except ValueError:
                print(f"{Fore.RED}You can only enter a whole number!{Fore.RESET}")

        if guess == product:
            print(f"{Fore.GREEN}EXCELLENT JOB! {fact_display}{product}\n")
            magicsound('qs - approval 6.wav')
            correct_answers += 1
            grand_total_right += 1
        else:
            print(f"{Fore.RED}OH NO!  INCORRECT. {fact_display}{product}\n")
            magicsound('qs - attention 6.wav')
            incorrect_answers += 1
            grand_total_wrong += 1


        questions_left -= 1
        print(f"{Fore.GREEN}Correct: {Fore.WHITE}{correct_answers} {Fore.RED}Incorrect: {Fore.WHITE}{incorrect_answers}")
        print(f"{Fore.CYAN}out of {Fore.YELLOW}{total_question_count - questions_left}{Fore.CYAN} total question.")

        print(f"\n{Fore.CYAN}You have {questions_left} questions remaining.\n")
        print(f"{Fore.MAGENTA}-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n")

    # Finished with all the questions of this game
    games_played += 1

    print(f"{Fore.GREEN}Great job, you completed another quiz!")
    print(f"\n{Fore.YELLOW}Would you like to go another round?")
    print(f"\n{Fore.CYAN}  Press {Fore.WHITE}[enter]{Fore.CYAN} to play again with {Fore.WHITE}{total_question_count}{Fore.CYAN} questions.")
    print(f"{Fore.CYAN}  Type {Fore.WHITE}'q'{Fore.CYAN} or {Fore.WHITE}'Q' {Fore.CYAN}to exit.")
    print(f"  Enter {Fore.WHITE}another number{Fore.CYAN} to play again with that question amount.")

    while (True):
        new_input = color_input(f"\n{Fore.CYAN}Enter {Fore.WHITE}'q', [enter], or a new question amount: ")
        if new_input.lower() == 'q':  
            print(f"\n{Fore.BLACK}{Back.LIGHTCYAN_EX}Thanks a lot for playing!  Enjoy your day!{Back.RESET}")
            print(f"\n{Fore.MAGENTA}=-=-=-=-=-=-=-=-=-=-=-=")
            print(f"=- {Fore.CYAN}GAMES PLAYED: {Fore.WHITE}{games_played}")
            print(f"{Fore.MAGENTA}=-=-=-=-=-=-=-=-=-=-=-=")
            print(f"=- {Fore.GREEN}Right Answers: {Fore.WHITE}{grand_total_right}")
            print(f"{Fore.MAGENTA}=- {Fore.RED}Wrong Answers: {Fore.WHITE}{grand_total_wrong}")
            print(f"{Fore.MAGENTA}=-=-=-=-=-=-=-=-=-=-=-=")
            magicsound("qs - goodbye quizzinator 6.wav")
            quit()

        if new_input == '':
            break

        if new_input.isdigit():
            total_question_count = int(new_input)
            print(f"Ok, restarting game with {total_question_count} questions!")
            break

        print(f"{Fore.RED}INVALID INPUT!")




       



